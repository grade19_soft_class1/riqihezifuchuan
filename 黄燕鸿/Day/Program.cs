﻿using System;

namespace Day
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateTime = DateTime.Now;
            Console.WriteLine("当前日期为：{0}", dateTime);
            Console.WriteLine("当前时本月的第{0}天", dateTime.Day);
            Console.WriteLine("当前是：{0}", dateTime.DayOfWeek);
            Console.WriteLine("当前是本年度第{0}天", dateTime.DayOfYear);
            Console.WriteLine("30 天后的日期是{0}", dateTime.AddDays(30));

        }
    }
}
