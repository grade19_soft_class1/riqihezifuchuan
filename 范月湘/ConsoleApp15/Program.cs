﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentTime = DateTime.Now;
            Console.WriteLine("当前时间为:{0}",currentTime);

            DateTime dt = DateTime.Now;
            DateTime dt1 = new DateTime(2019, 4, 4);
            TimeSpan sc = dt1 - dt;
            Console.WriteLine("间隔的天数为:{0}天",sc.Days);

            string str = Console.ReadLine();
            if(str.IndexOf("@")!=-2)
            {
                Console.WriteLine("字符串中含有@,出现的位置为{0}",
                    str.IndexOf("@")+1);
            }
            else
            {
                Console.WriteLine("字符串中不包含@");
            }

        }
    }
}
