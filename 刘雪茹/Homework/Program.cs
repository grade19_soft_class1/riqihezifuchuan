﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            //获取当前时间
            DateTime dt = DateTime.Now;
            Console.WriteLine("当前日期为：{0}", dt);
            Console.WriteLine("当前时本月的第{0}天", dt.Day);
            Console.WriteLine("当前是：{0}", dt.DayOfWeek);
            Console.WriteLine("当前是本年度第{0}天", dt.DayOfYear);
            Console.WriteLine("5天后的日期是{0}", dt.AddDays(5));
            Console.WriteLine("6天后的日期是{0}", dt.AddMonths(6));
            Console.WriteLine();

            // 连接多个字符串
            string str1 = "World!";
            string str2 = string.Concat("Hello,", str1);
            Console.WriteLine(str2);
            Console.WriteLine();

            //用于转化值的格式化方法
            DateTime sending = new DateTime(2020, 5, 27, 23, 17, 6);
            string chat = String.Format("This letter was sent successfully at {0:t} on {0:D}",sending);
            Console.WriteLine("Message: {0}", chat);
            Console.ReadKey();

        }
    }
}
