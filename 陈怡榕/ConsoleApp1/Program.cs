﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			DateTime dt1 = DateTime.Now;
			DateTime dt2 = new DateTime(2020, 06, 01);
			TimeSpan ts = dt2 - dt1;
			DateTime dt3 = DateTime.Now.AddDays(-30);


			Console.WriteLine("当前时间"+dt1);
			Console.WriteLine("今天：{0}",dt1.DayOfWeek);
			Console.WriteLine("30天前是"+dt3.Day+"号");
			Console.WriteLine("今年已经过了：{0}天了",dt1.DayOfYear);
			Console.WriteLine("距离今年六一儿童节还有"+ts.Days+"天"+ts.Hours+"小时");



			string str = "biubiubiu";

			Console.WriteLine(str.Substring(6, 2));

			Console.WriteLine(str.IndexOf("i"));

			Console.WriteLine(str.LastIndexOf("i"));

			Console.WriteLine(str.Length);

			int a = str.CompareTo("biubiu");
			if(a==0)
				{
				Console.WriteLine("相等");
			}
			else
			{
				Console.WriteLine("不相等");
			}



		}
	}
}
