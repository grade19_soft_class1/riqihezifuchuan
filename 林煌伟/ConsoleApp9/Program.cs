﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateTime = DateTime.Now;

            //获取该实例所表示的日期是一个月的第几天
            Console.WriteLine("现在是{0}号",dateTime.Day);
            Console.WriteLine();

            //获取该实例所表示的日期是一周的星期几
            Console.WriteLine("今天是"+dateTime.DayOfWeek);
            Console.WriteLine();

            //获取该实例所表示的日期是一年的第几天
            Console.WriteLine("现在是今年的第{0}天",dateTime.DayOfYear);
            Console.WriteLine();

            //获取实例的日期部分
            Console.WriteLine("现在的日期是"+dateTime);
            Console.WriteLine();

            //在指定的日期实例上添加指定天数
            Console.WriteLine("一个月后的现在是{0}",dateTime.AddDays(30));
            Console.WriteLine();

            //在指定的日期实例上添加指定的月份
            Console.WriteLine("一年后的现在是{0}",dateTime.AddMonths(12));
            Console.WriteLine();

            //在指定的日期实例上添加指定的年份
            Console.WriteLine("十年后的现在是{0}",dateTime.AddYears(10));
            Console.WriteLine();

            //时间差
            var dateTime1 = new DateTime(2020,10,1);
            var time = dateTime1 - dateTime;
            Console.WriteLine("现在到国庆还有{0}天",time.Days);
            
        }
    }
}
