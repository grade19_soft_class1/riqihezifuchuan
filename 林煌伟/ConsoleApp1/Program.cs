﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var str1 = "硕人其颀，衣锦褧衣。齐侯之子，卫侯之妻。东宫之妹，邢侯之姨，谭公维私。手如柔荑，肤如凝脂，领如蝤蛴，齿如瓠犀，螓首蛾眉，巧笑倩兮，美目盼兮。硕人敖敖，说于农郊。四牡有骄，朱幩镳镳。翟茀以朝。大夫夙退，无使君劳。河水洋洋，北流活活。施罛濊濊，鳣鲔发发。葭菼揭揭，庶姜孽孽，庶士有朅。";
            

            //获取字符串的长度
            Console.WriteLine("这篇诗一共有{0}个字",str1.Length+1);
            Console.WriteLine();

            //指定字符在原字符第一次出现的位置
            Console.WriteLine("人字第一次出现的位置是第{0}个字",str1.IndexOf("人")+1);
            Console.WriteLine();

            //指定字符在原字符最后一次出现的位置
            Console.WriteLine("人字最后出现的位置是第{0}个字",str1.LastIndexOf("人"));
            Console.WriteLine();

            //替换
            Console.WriteLine(str1.Replace("，","。"));
            Console.WriteLine();
            Console.WriteLine();

            // 截取字符串
            Console.WriteLine("形容庄姜夫人美丽的句子："+str1.Substring(35,35));
            Console.WriteLine();
            Console.WriteLine();

            //插入字符串
            var str2 = str1.Insert(0, "题目：硕人   ");
            Console.WriteLine("加了题目         "+str2);
            Console.WriteLine();
            Console.WriteLine();
            
            
            Console.WriteLine("原文"+str1);
        }   
    }
}
