﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //日期类
            DateTime dt = DateTime.Now;
            Console.WriteLine("当前的时间是{0}", dt);
            DateTime dt2 = new DateTime(2020, 1, 1);
            TimeSpan ts = dt - dt2;
            Console.WriteLine("间隔的天数为{0}天", ts.Days);
            Console.WriteLine(dt.DayOfWeek.ToString("d"));
            DateTime startWeek = dt.AddDays(1 - Convert.ToInt32(dt.DayOfWeek.ToString("d")));//本周周一
            DateTime endWeek = startWeek.AddDays(6);  //本周周日
            Console.WriteLine(startWeek);
            Console.WriteLine(endWeek);
            DateTime startMonth = dt.AddDays(1 - dt.Day);//本月月初
            Console.WriteLine(startMonth);
            DateTime Month = startMonth.AddMonths(-1);//上月月初
            Console.WriteLine(Month);
            Console.WriteLine("这日期花样太多了 掌握常用的就行 懒得去深究了哈哈哈");


            //            string字符串


            string str = Console.ReadLine();
            Console.WriteLine(str.Length);
            string a = "abacdefg    ";
            Console.WriteLine(a.IndexOf(a));
            Console.WriteLine(a.StartsWith ("c"));
            Console.WriteLine(a.ToUpper());
            Console.WriteLine(a.Remove (4));
            Console.WriteLine(a.Trim ()+"|||");
            char[] ch = { 'a', 'b', 'c', 'd' };
            Console.WriteLine(a.Trim (ch));
            Console.WriteLine(a.PadLeft(14));
            string str = "aaajbbbjccc";
            string[] sArray = str.Split('j');
            foreach (string i in sArray)
            {
                Console.WriteLine (i.ToString());
            }











        }
    }
}
