﻿using System;

namespace 日期和
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("现在的时间是： " + DateTime.Now.ToString("d"));
            DateTime time = DateTime.Now;

            Console.WriteLine("现在是这个月的第{0}天", time.Day);
            Console.WriteLine("当前是：{0}", time.DayOfWeek);
            Console.WriteLine("当前是本年度第{0}天", time.DayOfYear);
            Console.WriteLine("50 天后的日期是{0}", time.AddDays(50));

            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2020, 10, 1);
            TimeSpan ts = dt2 - dt1;

            Console.WriteLine();
            Console.WriteLine();
            var str = "你好哈哈哈";
            Console.WriteLine("这段文字为：" + str);
            var xb = str.IndexOf("看");
            Console.WriteLine("   看   在这段文字的第" + xb + "位(0开始算)");
            Console.ReadKey();
        }
    }
}
