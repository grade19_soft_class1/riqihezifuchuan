﻿using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {  
            //DateTime 
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2020, 06, 01);
            TimeSpan ts = dt2 - dt1;
            DateTime dt3 = DateTime.Now.AddDays(-30);

            Console.WriteLine("当前时间：" + dt1);
            Console.WriteLine("今天是：{0}", dt1.DayOfWeek);
            Console.WriteLine("30天前是" + dt3.DayOfWeek);
            Console.WriteLine("今年已经过了：{0}天了", dt1.DayOfYear);
            Console.WriteLine("距离今年的六一儿童节还有" + ts.Days + "天零" + ts.Hours + "小时");



            //string
            string str = "WANWMZYQBJHG";
             
            //截取字符串
            Console.WriteLine(str.Substring(9,3));

            //获取第一个w的索引位置
            Console.WriteLine(str.IndexOf("W"));

            //获取最后一个w的索引位置
            Console.WriteLine(str.LastIndexOf("W"));

            //字符串长度
            Console.WriteLine(str.Length);

            //判断相等
            int a = str.CompareTo("WANWMZYQB");
            if (a==0)
            {
                Console.WriteLine("两个字符串等");
            }
            else
            {
                Console.WriteLine("两个符串不相等");
            }

           
        }
    }
}
