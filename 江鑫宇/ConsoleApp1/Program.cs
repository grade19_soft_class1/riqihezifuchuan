﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
       

        //时间
            Console.WriteLine("Hello,这里是时间和字符串");
            DateTime time = DateTime.Now;
            Console.WriteLine("这是当前时间：{0}",time);
            Console.WriteLine("这是本月的第{0}天",time.Day);
            Console.WriteLine("这是{0}",time.DayOfWeek);
            Console.WriteLine("这是一年的第{0}天",time.DayOfYear);
            Console.WriteLine("30天后是什么：{0}",time.AddDays(30));
            DateTime time2 = new DateTime(2022, 9, 1);
            TimeSpan ts =  time2-time;
            Console.WriteLine("你好，间隔时间是：{0}天",ts.Days);


            //字符串
            String str = "我爱中国，中国牛B";
            Console.WriteLine("字符串长度为："+str.Length);
            Console.WriteLine("中国第一次出现的位置"+str.IndexOf("中"));
            Console.WriteLine("字符串第一个字符"+str[0]);
            Console.WriteLine("字符串最后一个字符"+str[str.Length-1]);
            Console.WriteLine("看下面的：");
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i]);
            }











        }
    }
}
