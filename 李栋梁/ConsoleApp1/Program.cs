﻿using System;
using System.ComponentModel.Design;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //当前日期和时间
            var xianzaishijian = DateTime.Now;
            Console.WriteLine(xianzaishijian.ToString("yyyy-MM-dd hh:mm:ss"));
            //只有日期
            Console.WriteLine(xianzaishijian.Date);
            //当前日期与转换
            var nowDate = "2020-05-29";
            //转换
            var nDAte = Convert.ToDateTime(nowDate);
            //后一天日期
            var sDate = nDAte.AddDays(+1);
            Console.WriteLine(sDate);
            //间隔日期
            DateTime one = xianzaishijian.Date;
            DateTime two = new DateTime(2001, 12, 28);
            TimeSpan rq = one - two;
            Console.WriteLine("我出生了{0}天", rq.Days);
            //字符串
            String str = "颜色不一样的烟火！！！！！";
            Console.WriteLine(str);
            Console.WriteLine("字符串的长度为：" + str.Length);
            Console.WriteLine("字符串中的第三个字为：" + str[2]);
            Console.WriteLine("字符串最后一个字符为：" + str[str.Length - 1]);
            //逆序输出
            String st ="dadadadasdad";
            for (int i = st.Length - 1; i >= 0; i--) 
            {
                Console.Write(str[i]);
            }
            //查找
            if (str.IndexOf("不") != -1)
            {
                Console.WriteLine("字符串中有 [不] ，它的位置是{0}", str.IndexOf("不") + 1);
            }
            else
            {
                Console.WriteLine("字符串中没[不]");
            }
            //替换
            st = str.Replace("!", "?");
       
            Console.WriteLine("替换后的字符为：" + st);
        }
    }
}
