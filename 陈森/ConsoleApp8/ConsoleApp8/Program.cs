﻿using System;
using System.Threading.Tasks.Dataflow;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentTime = DateTime.Now;
            Console.WriteLine(currentTime.ToString("北京时间：" + " hh：mm:ss:ms"));
            Console.WriteLine(currentTime.Date);


            var Day = "2020-5-27";
            var d = Convert.ToDateTime(Day);
            var MD = "2001-10-31";
            var md = Convert.ToDateTime(MD);
            var td = d - md;
            Console.WriteLine("我在人间凑日子的天数:" + td);


            /*字符串*/
            var str = "我在人间凑数的日子";
            Console.WriteLine(str);
            Console.WriteLine("字符串长度:{0}", str.Length);
            Console.WriteLine("字符串的第5个字为：{0}", str[4]);

           

        }
    }
}
