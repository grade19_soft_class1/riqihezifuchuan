﻿using System;

namespace DayAndTime
{
    class Program
    {
        static void Main(string[] args)
        {
            
           


            Console.WriteLine("请输入你想输入的文字");
            string str = Console.ReadLine();
            Console.WriteLine("你输入的字符串长度为{0}，第一个字符为{1}，最后一个字符为{2}",str.Length,str[0],str[str.Length-1]);


            Console.WriteLine("请输入年份");
            int year = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入月份");
            int month = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入日期");
            int day = int.Parse(Console.ReadLine());
            DateTime dateTime = new DateTime(year,month,day);
            Console.WriteLine("你输入的具体年月日为：{0}",dateTime);
        }
    }
}
