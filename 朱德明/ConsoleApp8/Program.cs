﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            ////DateTime类
            Console.WriteLine("输入一个年份");
            int year = int.Parse(Console.ReadLine());
            Console.WriteLine("输入一个月份");
            int months = int.Parse(Console.ReadLine());
            Console.WriteLine("输入几日");
            int day = int.Parse(Console.ReadLine());
            DateTime dateTime1 = new DateTime(year, months, day);
            //
            Console.WriteLine("现在的日期为：{0}", dateTime1);
            //一个月的第几天
            Console.WriteLine("现在是这个月的第{0}天", dateTime1.Day);
            //一周的星期几    
            Console.WriteLine("当前是：{0}", dateTime1.DayOfWeek);
            //一年的第几天
            Console.WriteLine("当前是本年度第{0}天", dateTime1.DayOfYear);
            //30 天后的日期
            Console.WriteLine("30 天后的日期是{0}", dateTime1.AddDays(30));
            DateTime dateTime2 = DateTime.Now;
            TimeSpan timeSpan = dateTime2 - dateTime1;
            Console.WriteLine("输入的日期与现在的北京时间间隔{0}天", timeSpan.Days);


            //获取字符串长度（string.Length）
            Console.WriteLine("输入一串字符串");
            string str = Console.ReadLine();
            Console.WriteLine("该字符串的长度=" + str.Length);
            Console.WriteLine("字符串的第一个字符为" + str[0]);
            Console.WriteLine("最后一个为" + str[str.Length - 1]);
            //将字符串倒序输出...
            for (int i = str.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(str[i]);
            }
            //IndexOf和LastIndexOf：查找字符串中的字符
            //查看输入的字符中有没有猪
            if (str.IndexOf("猪") != -1)
            {
                Console.WriteLine("字符串中有猪这个字,并且位置在{0}", str.IndexOf("猪") + 1);
            }
            else
            {
                Console.WriteLine("字符串里面没有猪这个字");
            }

            //Replace：字符串替换函
            Console.WriteLine("输入一个新的字符串");
            string str1 = Console.ReadLine();
            if (str.IndexOf("猪") != -1)
            {
                str.Replace("猪", "傻");
            }
            Console.WriteLine("替换后的字符串为" + str1);


            //Substring：字符串截取函数
            Console.WriteLine("输入你的准考号（准考号最后两位为考场）准考号为8位数");
            string Id = Console.ReadLine();
            Console.WriteLine("您的考场为" + Id.Substring(6));


            //Insert：字符串插入
            //在字符串第二的位置插入！@！@！@！@！@
            Console.WriteLine("输入字符串");
            string str2 = Console.ReadLine();
            str2 = str2.Insert(1, "!@!@!@!@!@!@!@");
            Console.WriteLine("新字符串为" + str2);
        }
    }
}
