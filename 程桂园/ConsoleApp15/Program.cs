﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("输入字符串");
            string str = Console.ReadLine();
            Console.WriteLine("字符串长度{0}，第一个字和第二个字{1}{2}", str.Length, str[0], str[str.Length - 1]);
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i]);
            }
            String str1 = str;
            str1 = str1.Insert(1, "student");
            Console.WriteLine(str1);


            var currentTime = DateTime.Now;
            Console.WriteLine("当前日期{0}", currentTime);

            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2020, 4, 24);
            TimeSpan ts = dt2 - dt1;
            Console.WriteLine("间隔的天数为{0}天", ts.Days);
        }
}
}