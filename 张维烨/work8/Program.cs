﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work8
{
    class Program
    {
        static void Main(string[] args)
        {
            #region//日期
            DateTime dt = DateTime.Now;
            Console.WriteLine("当前日期为：{0}", dt);
            Console.WriteLine("当前时本月的第{0}天", dt.Day);
            Console.WriteLine("当前是：{0}", dt.DayOfWeek);
            Console.WriteLine("当前是本年度第{0}天", dt.DayOfYear);
            Console.WriteLine("30 天后的日期是{0}", dt.AddDays(30));
            Console.WriteLine("一个月后的我是{0}", dt.AddMonths(1));
            Console.WriteLine("一年后的我哟{0}", dt.AddYears(1));

            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2020, 6, 1);
            TimeSpan ts = dt2 - dt1;
            Console.WriteLine("\n间隔的天数为{0}天\n", ts.Days);
            #endregion

            #region//字符串
            Console.WriteLine("请输入你的邮箱：");
            string str = Console.ReadLine();
            if (str.IndexOf("@") != -1)
            {
                Console.WriteLine("字符串中含有@，其出现位置是{0}", str.IndexOf("@") + 1);
            }
            else
            {
                Console.WriteLine("字符串中没有@");
            }
            Console.WriteLine();
        foruser:
            Console.WriteLine("请输入你的邮箱：");
            string str1 = Console.ReadLine();
            int firstindex = str1.IndexOf("@");
            int lastindex = str1.LastIndexOf("@");
            if (firstindex != -1)
            {
                if (firstindex == lastindex)
                {
                    Console.WriteLine("输入成功");
                    Console.WriteLine("你的QQ号码为：{0}",str1.Substring(0,firstindex));
                    
                }
                else
                {
                    Console.WriteLine("邮箱中含有多个@，请重新输入");
                    goto foruser;
                }
            }
            else
            {
                Console.WriteLine("邮箱格式错误，请重新输入");
                goto foruser;
            }
            Console.WriteLine("\n字符串替换（a换成@）\n");
            string str2 = Console.ReadLine();
            if (str2.IndexOf("a") != -1)
            {
                str2 = str2.Replace("a", "@");
            }
            Console.WriteLine("替换后的字符串为：" + str2);
            Console.WriteLine("\n字符串插入（在第二个位置插入@@@）\n");
            string str3 = Console.ReadLine();
            str3 = str3.Insert(1, "@@@");
            Console.WriteLine("新字符串为：" + str3);
            #endregion
            Console.ReadKey();
        }
    }
}
