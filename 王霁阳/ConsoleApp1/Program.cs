﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{
			//显示现在时间
			var Time = DateTime.Now;
			Console.WriteLine("现在是北京时间{0}", Time);
			//显示今天是星期几
			Console.WriteLine("今天是星期{0}", Time.DayOfWeek);
			//显示今天是今年天
			Console.WriteLine("今天是今年的第{0}", Time.DayOfYear);
			//显示30天之后的天数
			Console.WriteLine("30天之后是{0}", Time.AddDays(30));
			//两个时间的间隔
			var Time1 = new DateTime(2019, 10, 21);
			var Time2 = new DateTime(2018, 2, 10);
			TimeSpan TS = Time1 - Time2;
			Console.WriteLine("它们的时差为{0}", TS);
			Console.WriteLine("他们的时差用秒来表示为{0}", TS.TotalSeconds);
			//定义一个字符串
			Console.WriteLine("请输入一串字符");
			var String1 = Console.ReadLine();
			Console.WriteLine("你输入的字符串为{0}", String1);
			Console.WriteLine("你输入的字符串长度为{0}", String1.Length);
			Console.WriteLine("你输入的第一个字符为{0}", String1[0]);
			Console.WriteLine("你输入的最后一个字符为{0}",String1[String1.Length-1]);
			var String2 = "abcdefghijklon";
			//逆序输出
			Console.WriteLine("字符串2为{0}",String2);
			Console.WriteLine("字符串2逆序输入为");
			for(int i = String2.Length - 1; i >= 0; i--)
			{
				Console.Write(String2[i]);
			}
			Console.WriteLine("字符c在字符串2里面第一次出现在{0}的位置",1+(String2.IndexOf("c")));
		}
	}
}
