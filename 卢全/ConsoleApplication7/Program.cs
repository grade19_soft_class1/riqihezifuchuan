﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication7
{
    class Program
    {
        static void Main(string[] args)
        {

            //DateTime 时间
            Console.WriteLine( "请输入年份");
            int year = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入月份");
            int month = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入日期");
            int day = int.Parse(Console.ReadLine());

            DateTime dt = new DateTime(year, month, day);


            Console.WriteLine("当前日期为：{0}",dt);

            Console.WriteLine("今天星期：{0}",dt.DayOfWeek);

            Console.WriteLine("现在是一年中的第几天：{0}",dt.DayOfYear);

            Console.WriteLine("现在是{0}年",dt.AddDays(30));  


            DateTime dt1 =new DateTime(2011,2,3);
            TimeSpan ts = dt - dt1;
            Console.WriteLine("间隔{0}天",ts.Days);


            //Length 长度

            Console.WriteLine("请输入");

            string str = string.Intern(Console.ReadLine());

            Console.WriteLine("字符串的长度为：{0}",str.Length);

            Console.WriteLine("第一个字符为：{0}",str[0]);

            //字符串逆序输出
            for (int i = str.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(str[i]);
            }

            //查找位置and替换and插入
            if (str.IndexOf("小") != -1)
            {   //  位置
                Console.WriteLine("字符串中含有 小 ，位置是{0}", str.IndexOf("小") + 1);
                //替换
                str = str.Replace("小", "大");
                //插入
                str = str.Insert(0, "可爱的");
            }
            else
            {
                Console.WriteLine("不存在");
            }
            
            //冒泡排序
            //大到小排序
            int[] a = { 2, 4, 1, 6, 5 };
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < a.Length - i - 1; j++)
                {
                    if (a[j] < a[j + 1])
                    {
                        int temp = a[j];
                        a[j] = a[j + 1];
                        a[j+1]=temp;
                    }
                }
            }
             foreach(int b in a)
          {
             Console.Write(b + "");
          }
             Console.WriteLine();

            //Sort  小到大
             int[] e = { 2, 8, 1, 9, 4 };
             Array.Sort(e);
            foreach(int w in e)
            {
                Console.Write(w);
            }
            Console.WriteLine();
            
    }
  }
 }

