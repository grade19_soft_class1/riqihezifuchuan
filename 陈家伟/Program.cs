﻿using System;

namespace 日期和字符串
{
    class Test
    {
        static void Main(string[] args)
        {
            /*TimeSpan ts = new TimeSpan();
            TimeSpan ts1 = TimeSpan.FromMinutes(10);
            TimeSpan ts2 = TimeSpan.Parse("1:20:00");
            Console.WriteLine(ts1);
            Console.WriteLine(ts2);*/
            string st1 = "Hello World!";
            string st2 = "Hi!Welcome!";
            if (string.Compare(st1, st2) == 0)
            {
                Console.WriteLine(st1 + " and " + st2 + "are equal");
            }
            else
            {
                Console.WriteLine(st1 + " and " + st2 + "are not equal");
            }
            Test test = new Test();
            test.Sentence();
            test.Sentence1();
            test.Sentence3();
            test.Sentence4();
            test.Sentence5();
            test.Sentence6();
            test.Sentence7();
            
        }
        public  void  Sentence()
        {
            string string1 = "Today have a lot of thing to do in longyan";
            Console.WriteLine(string1);
            string str = string1.Substring(35);
            Console.WriteLine(str);
        }
        public void Sentence1()
        {
            string FirstName, LastName;
            FirstName = "Zhang";
            LastName = "Shan";
            string FullName = FirstName + LastName;
            Console.WriteLine("这个人全名叫:"+FullName);

        }
        public void Sentence3()
        {
            char[] Place = {'L','o','n','g','Y','a','n' };
            string place1 = new string(Place);
            Console.WriteLine("The place called :"+place1);
        }
        public void Sentence4()
        {
            string[] Tourist = {"China","to","London" };
            string connection = string.Join("  ", Tourist);
            Console.WriteLine(connection);
            
        }
        public void Sentence5()
        {
            DateTime now = new DateTime(2020,5,28,11,26,0);
            string chat = string.Format("Message send {0:T} on {0:D}",now);
            Console.WriteLine(chat);
            Console.ReadKey();


        }
        public void Sentence6()
        {
            string [] array = new string [] { "I saw a pig",
                "I saw a cat",
                "I saw a dog",
                "I saw a dragon" };
            string animal = String.Join("\n", array);
            Console.WriteLine(animal);
            Console.ReadKey();
        }
        public void Sentence7()
        {
            var Time = DateTime.Now;
            var FutureTime = new DateTime(2020, 7, 1);
            var DifferTime = FutureTime - Time;
            Console.WriteLine(DifferTime.TotalDays);
            Console.WriteLine(DifferTime.TotalHours);
            Console.WriteLine(DifferTime.Days);
            Console.WriteLine(DifferTime.Hours);
        }
        public void Sentence8()
        {
            var FirstDay = "2020-05-28";
            var Fday = Convert.ToDateTime(FirstDay);
            var Lday = Fday.AddDays(+1);
            Console.WriteLine(Fday);
        }



    }
}
