﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Runtime.Serialization.Formatters;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //时间输出
            var str = DateTime.Now;
            Console.WriteLine(str.ToString("现在是北京时间:hh:mm:ss"));


            //出生日期计算
            var firstDate = "2000-12-15";
            var fd = Convert.ToDateTime(firstDate);
            var lastDate = "2020-5-26";
            var ld = Convert.ToDateTime(lastDate);
            var bd = ld - fd;
            Console.WriteLine("你已出生" + bd + "天");


            //替换
            string s = "今夕复何夕，共此明月光";
            s = s.Replace("明", "白");
            Console.WriteLine(s);


            //截取
            string x = "南方晴空万里，北方大雪纷飞";
            x = x.Substring(2,4);
            Console.WriteLine(x);


            //插入
            string f = "桃之夭夭";
            f = f.Insert(4,"，灼灼其华");
            Console.WriteLine(f);


            //逆序
            string n = "庭园满香花";
            for (int i = n.Length - 1; i >= 0; i--)
            {
                Console.Write(n[i]);
            }


            //查找
            string k = "天山三丈雪";
            var l = k.IndexOf("雪");
            Console.WriteLine(l);
            Console.WriteLine(k.LastIndexOf("山"));
        }
    }
}
