﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dates
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("现在的时间是（短）： "+DateTime.Now.ToString("d"));
            DateTime time = DateTime.Now;
            Console.WriteLine("现在的时间是（长）：" + time);

            Console.WriteLine("当前时本月的第{0}天", time.Day);
            Console.WriteLine("当前是：{0}", time.DayOfWeek);
            Console.WriteLine("当前是本年度第{0}天", time.DayOfYear);
            Console.WriteLine("30 天后的日期是{0}", time.AddDays(30));

            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2020, 10, 1);
            TimeSpan ts = dt2 - dt1;
            Console.WriteLine("距离下次国庆还有{0}天", ts.Days);

            Console.WriteLine();
            Console.WriteLine();
            var str = "小波最好看啦";
            Console.WriteLine("这段文字为："+str);
            var xb = str.IndexOf("看");
            Console.WriteLine( "   看   在这段文字的第"+ xb +"位(0开始算)");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("要在此字段里插入“屁屁最喜欢了”");
            var str1 = str.Insert(6,",屁屁最喜欢了");
            Console.WriteLine("插入后的字段为：  " + str1);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("就演示这样吧，下回继续");
        }
    }
}
