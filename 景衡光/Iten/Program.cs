﻿using System;

namespace Iten
{
    class Program
    {
        static void Main(string[] args)
        {
            //DateTime
            Console.WriteLine("请输入一个年份");
            int year = int.Parse(Console.ReadLine());

            DateTime dt = DateTime.Now;
            DateTime birthday = new DateTime(2020, 11, 12);
            TimeSpan ts = birthday - dt;
            bool a = DateTime.IsLeapYear(year);

            Console.WriteLine("现在是北京时间:" + dt.ToString("hh:mm:ss"));
            Console.WriteLine("距离我的生日还有{0}天", ts.Days);
            if (a == true)
            {
                Console.WriteLine("{0}年是闰年", year);
            }
            else
            {
                Console.WriteLine("{0}年不是闰年", year);
            }


            //string
            string str = "在贫瘠的土地上，你是我最后的玫瑰";

            Console.WriteLine("字符串长度:"+str.Length);
            Console.WriteLine("我爱你"+str.Substring(14));
            Console.WriteLine(str.IndexOf("，"));
            Console.WriteLine(str.Remove(11,2));
            Console.WriteLine(str.Replace("贫瘠","我爱"));
           

        }
    }
}
