﻿using System;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            
                string str = Console.ReadLine();
                Console.WriteLine("字符串的长度为：" + str.Length);
                Console.WriteLine("字符串中第一个字符为：" + str[0]);
                Console.WriteLine("字符串中最后一个字符为：" + str[str.Length - 1]);
            
  
        }
    }
}
