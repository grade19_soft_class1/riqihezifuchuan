﻿using System;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dt = DateTime.Now;
            Console.WriteLine("当前日期为：{0}", dt);
            Console.WriteLine("当前时本月的第{0}天", dt.Day);
            Console.WriteLine("当前是：{0}", dt.DayOfWeek);
            Console.WriteLine("当前是本年度第{0}天", dt.DayOfYear);
            Console.WriteLine("30 天后的日期是{0}", dt.AddDays(30));
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = new DateTime(2019, 6, 1);
            TimeSpan ts = dt2 - dt1;
            Console.WriteLine("间隔的天数为{0}天", ts.Days);
        }
    }
}
