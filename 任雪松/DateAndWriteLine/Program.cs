﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateAndWriteLine
{
    class Program
    {
        static void Main(string[] args)
        {
            //时间和日期
            DateTime date= DateTime.Now;
            Console.WriteLine("现在是北京时间：" + date.ToString("yyyy-mm-dd  hh:mm:ss"));
            Console.WriteLine("现在是这个月的第{0}天", date.Day);
            var date0 = "2020 - 5 - 28";
            var nowday = Convert.ToDateTime(date0);
            var mybirthday = "2001 - 1 - 8";
            var myday = Convert.ToDateTime(mybirthday);
            var day = nowday - myday;
            Console.WriteLine("我已经出生了" + day + "天");
            DateTime date1 = DateTime.Now;
            DateTime date2 = new DateTime(2021, 01, 08);
            TimeSpan bt = date2 - date1;
            Console.WriteLine("距离我的二十岁生日还有{0}天", bt.Days);

            //字符串
            String str = "布丁是一只小英短";
            Console.WriteLine(str);
            Console.WriteLine("这段字符串的长度为：" + str.Length);
            //插入字符串
            String str1 = str.Insert(8, ",小布丁最可爱。");
            Console.WriteLine(str1);
            for(int i=str1.Length-1;i>=0;i--)
            {
                Console.Write(str1[i]);
            }
            Console.WriteLine();
            //遍历字符串
            for( int i=0;i < str.Length;i++)
            {
                Console.WriteLine(str[i] + ",");
            }

            String str2 = "Hello,Word!!你好么？";
            Console.WriteLine(str2);
            Console.WriteLine(str2.ToLower());//字符串英文换成小写
            Console.WriteLine(str2.ToUpper());//字符串英文换成大写
            Console.WriteLine(str2.IndexOf("好"));
            Console.WriteLine(str2.LastIndexOf("么"));//IndexOf和LadtIndexOf都是查找字符串中字符的位置
            //替换字符串
            String name = "任雪松真丑！";
            name = name.Replace("丑", "帅");
            Console.WriteLine(name);
            bool A = name.Contains("任");
            Console.WriteLine("字符串中是否包含‘任’："+A);
        }
    }
}
