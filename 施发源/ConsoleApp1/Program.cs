﻿using System;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //当前时间
            var xzdate = DateTime.Now;
            Console.WriteLine($"现在是北京时间：{xzdate.ToString("hh:mm:ss")}");
            //当前日期
            string rq = xzdate.Date.ToString("yyyy-MM-dd");
            Console.WriteLine($"今天是：{rq}");
            //替换日期显示的样式
            Console.WriteLine($"今天是：{rq.Replace("-","/")}");
            //日期及时间显示的长度
            string cd = xzdate.ToString("yyyy-mm-dd hh:mm:ss");
            Console.WriteLine($"当前日期的长度为：{cd.Length}");
            //字符串截取(截取月)
            string yue = xzdate.ToString("yyyy-MM-dd");
            yue = yue.Substring(5,2);
            //今天是一个月的第几天
            Console.WriteLine($"今天是{yue}月的第{xzdate.Day}天");
            string xq =xzdate.DayOfWeek.ToString("");
            //今天是星期几
            Console.WriteLine($"今天是{xq}");
            //将星期里的大写字母改小写
            string d = xq.ToLower();
            Console.WriteLine($"大改小{d}");
            //将星期里的小写字母改大写
            string x = xq.ToUpper();
            Console.WriteLine($"小改大{x}");
            //在星期前插入日期和时间
            xq = xq.Insert(0, xzdate.ToString("yyyy年MM月dd日 hh:mm:ss "));
            Console.WriteLine(xq);
            //今天是今年的第几天
            Console.WriteLine($"今天是今年的第{xzdate.DayOfYear}天");
			//在当前时间加2天，2小时，2分，2秒
			var h = new System.TimeSpan(2, 2, 2, 2);
			Console.WriteLine($"2天2小时，2分，2秒后的时间{xzdate.Add(h)}");
			
			
			//2天后的日期是
			Console.WriteLine($"2天后的日期为：{xzdate.AddDays(2).ToString("yyyy-MM-dd")}");
			//2小时后的时间
			Console.WriteLine($"2小时后的时间为：{xzdate.AddHours(2).ToString("hh:mm:ss")}");
			//2分钟后的时间
			Console.WriteLine($"2小时后的时间为：{xzdate.AddMinutes(2).ToString("hh:mm:ss")}");
			//2秒后的时间
			Console.WriteLine($"2小时后的时间为：{xzdate.AddSeconds(2).ToString("hh:mm:ss")}");
			


		}
	}
}
