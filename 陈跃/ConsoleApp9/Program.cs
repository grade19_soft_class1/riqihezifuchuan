﻿using System;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime time = DateTime.Now;
            Console.WriteLine("当前时间为：{0}",time);
            Console.WriteLine("当前时间是这个月的第{0}天", time.Day);
            Console.WriteLine("当前时间是今天的第{0}天",time.DayOfYear);
            Console.WriteLine("当前时间后的第五十天是{0}",time.AddDays(50));
            Console.WriteLine();

            DateTime time1 = DateTime.Now;
            var dt = new DateTime(2020, 06, 25);
            TimeSpan ts = dt - time1;
            Console.WriteLine("距离端午节还有{0}天",ts.Days);
            Console.WriteLine();

            var str = "闽西职业技术学院";
            Console.WriteLine(str);
            Console.WriteLine("字符串的长度为:{0}",str.Length);
            Console.WriteLine("字符串中的第5个字为：{0}",str[4]);

            var str1 = str.Insert(8, "软件一班");
            Console.WriteLine(str1);

        }
    }
}
