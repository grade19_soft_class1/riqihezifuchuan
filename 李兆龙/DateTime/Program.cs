﻿using System;
using System.Threading;
using Microsoft.VisualBasic.CompilerServices;

namespace DayandString
{
    class Program
    {
        static void Main(string[] args)
        {
            //目前年月日时间
            var stri = DateTime.Now;
            Console.WriteLine("现在是北京时间:" + stri.ToString("yyyy-mm-dd hh:mm:ss"));

            //目前年月日
            Console.WriteLine(stri.Date);

            //计算年龄
            var isDay = 2020 - 5 - 26;
            var myBirth = 2000 - 7 - 25;
            var isBirth = isDay - myBirth;
            Console.WriteLine("我的年龄是：" + isBirth + "岁");

            //出生天数
            var nowDay = "2020-5-26";
            var nd = Convert.ToDateTime(nowDay);
            var myDay = "2000-5-26";
            var md = Convert.ToDateTime(myDay);
            var td = nd - md;
            Console.WriteLine("我已经出生了" + td + "天");

            //字符串长度
            string str = " Hello Baby I love you ";
            Console.WriteLine("字符串的长度Length：" + str.Length);

            //遍历字符串
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i] + ",");
            }

            //将字符串大写转换成小写
            Console.WriteLine(str.ToLower());

            //将字符串小写转换成大写
            Console.WriteLine(str.ToUpper());

            //将字符串转换成char数组
            char[] Arr = str.ToCharArray();
            Console.WriteLine(Arr);

            Console.WriteLine("去除头尾空格Trim" + str.Trim());
            Console.WriteLine("去除头空格Trim" + str.TrimStart());
            Console.WriteLine("去除尾空格Trim" + str.TrimEnd());

            //是否包含此内容
            bool isY = str.Contains("Hello");
            Console.WriteLine("查找字符串中是否包含Contains:" + isY);

            //替换内容
            string stence = "瘟世间情为何物";
            stence = stence.Replace("瘟", "问");
            Console.WriteLine(stence);

            //插入内容
            String isStence = "七月飞";
            isStence = isStence.Insert(3, "雪");
            Console.WriteLine(isStence);

            //截取字符串  参数1：从哪个位置开始截，参数2：截取的长度，如果不指定长度，就直接截取到字符串结束 
            string goodStence = "毛主席领导咱们打江山";
            goodStence = goodStence.Substring(7); //从零开始，包括第七位及后面的数
            Console.WriteLine(goodStence);

            //倒叙
            var niceStence = "当朋友问你关于我";
            for (int i = niceStence.Length - 1; i > 0; i--)
            {
                Console.Write(niceStence[i]);
            }

            //根据某一个字符，切割开字符串，返回字符串的数组
            string[] strArr = str.Split(' ');
            for (int i = 0; i < strArr.Length; i++)
            {
                Console.WriteLine(strArr[i]);
            }

            //IndexOf和LastIndexOf：查找字符串中的字符
            var beauStence = "连借口我都帮你寻";
            Console.WriteLine(beauStence.IndexOf("寻"));
            Console.WriteLine(beauStence.LastIndexOf("寻"));
        }
    }
}
