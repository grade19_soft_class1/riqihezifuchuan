﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //时间日期
            DateTime dtme = DateTime.Now;
            Console.WriteLine("当前日期为{0}", dtme.Date);
            Console.WriteLine("当前是本月的第{0}天", dtme.Day);
            Console.WriteLine("当前是本年的第{0}天", dtme.DayOfYear);
            Console.WriteLine("30天后的日期是{0}", dtme.AddDays(30));
            DateTime date1 = DateTime.Now;
            DateTime date2 = new DateTime(2019, 6, 24);
            TimeSpan timeSpan = date1 - date2;
            Console.WriteLine("过去了{0}天", timeSpan.Days);

            //字符串
            string str = Console.ReadLine();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(str[i]);
            }
            //字符串替换
            string wenan = "‘我与春风皆过客，你携秋水揽星河’";
            if (wenan.IndexOf("，")!= -1)
            {
                wenan = wenan.Replace("，", "-");
            }
            Console.WriteLine("替换后的字符串为："+ wenan);

            Console.WriteLine("请输入QQ邮箱");
            string email = Console.ReadLine();
            if (email.IndexOf("@") != -1)
            {
                Console.WriteLine("你的邮箱是{0}", email);
                Console.WriteLine("请确认您的QQ邮箱\n请输入“确认”");
                string confirm = Console.ReadLine();
                if (confirm == "确认")
                {
                    Console.WriteLine("认证邮箱成功！");
                }
                else
                {
                    Console.WriteLine("请输入“确认”");
                }
            }
            else
            {
                Console.WriteLine("请输入正确的QQ邮箱");
            }
            //字符串截取
            Console.WriteLine("请输入您的邮箱。");
            string user = Console.ReadLine();
            int firstIndex = user.LastIndexOf("@");
            int lastIndex = user.LastIndexOf("@");
            if(firstIndex != -1 && firstIndex == lastIndex)
            {
                user = user.Substring(0, firstIndex);
            }
            Console.WriteLine("邮箱中的用户名是：" + user);
        }
    }
}
