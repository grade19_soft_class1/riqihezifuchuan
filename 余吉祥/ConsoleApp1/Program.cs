﻿using System;

namespace ConsoleApp1
{
     
    class Program
    {
        static void Main(string[] args)

        {

            //在 Main 方法中从控制台输入一个字符串，
            //输出该字符串的长度，以及字符串中的第一个字符和最后一个字符。
            Console.WriteLine("请输入一个字符串");
            string str = Console.ReadLine();
            Console.WriteLine("这个字符串的长度为：{0}", str.Length);
            Console.WriteLine("这个字符串的第一个字符是{0}", str[0]);
            Console.WriteLine("这个字符串的最后一个字符是{0}", str[str.Length - 1]);


            //在 Main 方法中从控制台输入一个字符串，并将字符串中的字符逆序输出。
            Console.WriteLine("请输入一字符串");
            string sr = Console.ReadLine();
            Console.Write("逆序输出的结果为：");
            for (int i = 0; i < sr.Length;i++)
            {
                Console.Write(sr[sr.Length-i-1]);
            }


            //在 Main 方法中从控制台输入一个字符串，然后判断字符串中是否含有 @, 
            //并输出 @ 的位置。
            Console.WriteLine("请输入一个字符串：");
            string a = Console.ReadLine();

                if (a.IndexOf("@")!=-1)
                {
                    Console.WriteLine("字符串中有@，@的位置是{0}",a.IndexOf("@")+1);
                }
                else
                {
                    Console.WriteLine("字符串中没有@");
                }


            //在 Main 方法中从控制台输入一个字符串，判断该字符串中是否仅含有一个 @。
            Console.WriteLine("请输入一个字符串");
            string b = Console.ReadLine();
            if (b.IndexOf("@")!=-1) {
               if (b.IndexOf("@") == b.LastIndexOf("@"))
               {
                    Console.WriteLine("字符串中只有一个@");
                }
                else
                {
                    Console.WriteLine("字符串中不止一个@");
                }
            }
            else
            {
                Console.WriteLine("字符串中没有@");
            }


            //在 Main 方法中从控制台输入一个字符串，然后将字符串中所有的‘,’替换成‘_’。
            Console.WriteLine("请输入一个字符串");
            string c = Console.ReadLine();
            if (c.IndexOf(",")!=-1)
            {
                 c=c.Replace(",", "-");
            }
            Console.WriteLine("替换后的字符串为{0}",c);


            //在 Main 方法中从控制台输入邮箱，要求邮箱中仅含有一个 @，
            //然后截取邮箱中的用户名输出。
            Console.WriteLine("请输入邮箱");
            string d = Console.ReadLine();
            if (d.IndexOf("@")!=-1)
            {
                if (d.IndexOf("@") == d.LastIndexOf("@"))
                {
                    d = d.Substring(0, d.IndexOf("@"));
                    Console.WriteLine("用户名是：{0}",d);
                }
            }


            //使用 DateTime 类获取当前时间，分别输出该日是当月的第几天、
            //星期几以 及一年中的第几天，并计算 30 天后的日期。
            DateTime dt = DateTime.Now;
            Console.WriteLine("当前日期为：{0}", dt);
            Console.WriteLine("当前时本月的第{0}天", dt.Day);
            Console.WriteLine("当前是：{0}", dt.DayOfWeek);
            Console.WriteLine("当前是今年第{0}天", dt.DayOfYear);
            Console.WriteLine("30 天后的日期是{0}", dt.AddDays(30));
        }
    }
}
