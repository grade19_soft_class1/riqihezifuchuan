﻿using System;

namespace Date
{
    class Program
    {
        static void Main(string[] args)
        {
           var str1 = "上海自来水来自海上";
            Console.WriteLine("长度："+str1.Length);
            Console.WriteLine("第五个字符："+str1[4]);
            Console.WriteLine("尾巴：" + str1[str1.Length - 1]);
            Console.WriteLine();

           
            //打印字符串每个字
            for (int i = 0; i < str1.Length; i++)
            {
                Console.WriteLine("第" + (i + 1) + "个字：" + str1[i]);

            }
            
            Console.WriteLine();
            //字符串倒序
            string str2 = "前任只认钱";


            for (int j = str2.Length - 1; j > 0; j--)
            {
                Console.WriteLine("第" + j + "个字：" + str2[j]);
            }
            var indexStr = str2.IndexOf("认");
            Console.WriteLine("认 在第"+(indexStr+1)+"位,"+"下标为："+indexStr);//查找 认 字是否存在

            Console.WriteLine(str2.LastIndexOf("认"));//认 字下标

            Console.WriteLine(str2.IndexOf("刘"));//-1 为不存在

            Console.WriteLine();
            //字符串替换
            var str3 = str2.Replace("只","不会不");
            Console.WriteLine(str3);

            Console.WriteLine();
            //字符串截取
            var str4 = str3.Substring(4);//我要截取str3中的第四字以后个字
            Console.WriteLine(str4);

            var str5 = str3.Substring(3,1);//提取出下标为3 长度为1的字符串
            Console.WriteLine(str5);

            
            Console.WriteLine();
            //字符串插入
            var str6 = str1.Insert(5, "不全都");
            Console.WriteLine(str6);



            Console.WriteLine();
            //日期时间
            var nowTime=DateTime.Now;
            Console.WriteLine("当前时间：" + nowTime);//跟随系统时间样式

            Console.WriteLine(nowTime.ToString("yy-MM-dd hh:ss:mm"));//自定义日期时间样式
            Console.WriteLine(nowTime.ToString ("yyyy-MM-dd"));
            Console.WriteLine(nowTime.Date);//单独日期

            Console.WriteLine();
            //关于月份 
            var firstDate = "2020-3-01";//假设现在为三月一日
            var fDate = Convert.ToDateTime(firstDate);
            var lDate = fDate.AddDays(-1);//上个月最后一天
            Console.WriteLine(lDate);//平闰年不影响 

            Console.WriteLine("上个月总共有："+lDate.Day+"天");//也可以说是上个月第几天

            Console.WriteLine("现在是一年的第："+lDate.DayOfYear+"天");

            Console.WriteLine("6个月后的："+lDate.AddMonths(6));

            Console.WriteLine();
            //时间间隔
            var dt = new DateTime(2020, 06, 01);
            var tStamp = dt - nowTime;
            Console.WriteLine(tStamp.TotalDays);


            //强制类型转换

            int l = 666;
            double k = l;
            Console.WriteLine(k.ToString("0.000"));



        }
    }
}
